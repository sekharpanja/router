import unittest

from router.digraph import Digraph
from router.processor import Processor


class TestShortestPath(unittest.TestCase):
    def setUp(self):
        # setup graph object to inspect
        self.graph = Digraph()
        for node in ['NY', 'LP', 'CB', 'CT', 'BA']:
            self.graph.add_node(node)

        self.graph.add_edge('BA', 'NY', 6)
        self.graph.add_edge('BA', 'CB', 5)
        self.graph.add_edge('BA', 'CT', 4)
        self.graph.add_edge('NY', 'LP', 4)
        self.graph.add_edge('LP', 'CB', 3)
        self.graph.add_edge('LP', 'CT', 6)
        self.graph.add_edge('CB', 'LP', 3)
        self.graph.add_edge('CB', 'CT', 6)
        self.graph.add_edge('CT', 'NY', 8)

    def test_journey_time_BA_NY_LP(self):
        days = Processor.get_journey_time(self.graph, ['BA', 'NY', 'LP'])
        self.assertEqual(10, days)

    def test_journey_time_BA_CB_LP(self):
        days = Processor.get_journey_time(self.graph, ['BA', 'CB', 'LP'])
        self.assertEqual(8, days)

    def test_journey_time_BA_CT_NY_LP_CB(self):
        days = Processor.get_journey_time(self.graph, ['BA', 'CT', 'NY', 'LP', 'CB'])
        self.assertEqual(19, days)

    def test_journey_time_BA_CT_CB(self):
        days = Processor.get_journey_time(self.graph, ['BA', 'CT', 'CB'])
        self.assertIsNone(days)

    def test_shortest_BA_LP(self):
        days, route = Processor.find_shortest_route(self.graph, 'BA', 'LP')
        self.assertEqual(8, days)
        self.assertEqual(['BA', 'CB', 'LP'], route)

    def test_shortest_NY_NY(self):
        days, route = Processor.find_shortest_route(self.graph, 'NY', 'NY')
        self.assertEqual(18, days)
        self.assertEqual(['NY', 'LP', 'CT', 'NY'], route)

    def test_shortest_NY_BA_None(self):
        self.assertIsNone(Processor.find_shortest_route(self.graph, 'NY', 'BA'))

    def test_routes_max_3_stops_LP_LP_lambda(self):
        routes = Processor.find_routes(self.graph, 'LP', 'LP', condition=lambda l: len(l) <= 3)
        self.assertEqual(2, len(routes))

    def test_routes_max_3_stops_LP_LP_op(self):
        routes = Processor.find_routes(self.graph, 'LP', 'LP', relate='<=', limit=3)
        self.assertEqual(2, len(routes))

    def test_routes_exactly_4_stops_BA_LP_lambda(self):
        routes = Processor.find_routes(self.graph, 'BA', 'LP', condition=lambda l: len(l) == 4)
        self.assertEqual(1, len(routes))

    def test_routes_exactly_4_stops_BA_LP_op(self):
        routes = Processor.find_routes(self.graph, 'BA', 'LP', relate='=', limit=4)
        self.assertEqual(1, len(routes))

    def test_routes_LP_BA_None(self):
        routes = Processor.find_routes(self.graph, 'LP', 'BA')
        self.assertEqual([], routes)

    def test_routes_max_25_days_LP_LP(self):
        routes = Processor.find_routes_limit_journey_time(self.graph, 'LP', 'LP', 25)
        self.assertEqual(3, len(routes))

    def test_routes_max_20_days_LP_LP(self):
        routes = Processor.find_routes_limit_journey_time(self.graph, 'LP', 'LP', 20)
        self.assertEqual(2, len(routes))

    def test_routes_journey_time_LP_BA_None(self):
        routes = Processor.find_routes_limit_journey_time(self.graph, 'LP', 'BA')
        self.assertEqual([], routes)
