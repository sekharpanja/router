import unittest
from router.digraph import Digraph


class TestDigraph(unittest.TestCase):

    def test_empty_digraph(self):
        d = Digraph()
        self.assertFalse(d.edges)
        self.assertFalse(d.nodes)
        self.assertFalse(d.weights)

    def test_add_node(self):
        d = Digraph()
        d.add_node('ABCD')
        self.assertEqual(1, len(d.nodes))
        self.assertTrue('ABCD' in d.nodes)

    def test_add_edge_after_node(self):
        d = Digraph()
        d.add_node('AB')
        d.add_node('CD')
        d.add_edge('AB', 'CD', 5)
        self.assertEqual(1, len(d.edges))
        self.assertEqual(1, len(d.weights))
        self.assertTrue('CD' in d.edges['AB'])
        self.assertEqual(5, d.weights[('AB', 'CD')])

    def test_add_edge_before_node(self):
        d = Digraph()
        d.add_edge('AB', 'CD', 5)
        self.assertEqual(1, len(d.edges))
        self.assertEqual(1, len(d.weights))
        self.assertTrue('CD' in d.edges['AB'])
        self.assertEqual(5, d.weights[('AB', 'CD')])

