This is an attempt to solve various flavours of TSP with Dijkstra and other algorithms.

ref: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

### SCENARIO:

    #### Nodes:
    
    * New York: 'NY', 
    * Liverpool: 'LP', 
    * Casablanca: 'CB', 
    * Cape Town: 'CT', 
    * Buenos Aires: 'BA'
        
    #### Distances in number of days:
    
    * ('BA', 'NY', 6)
    * ('BA', 'CB', 5)
    * ('BA', 'CT', 4)
    * ('NY', 'LP', 4)
    * ('LP', 'CB', 3)
    * ('LP', 'CT', 6)
    * ('CB', 'LP', 3)
    * ('CB', 'CT', 6)
    * ('CT', 'NY', 8)


### REQUIREMENTS:

    The challenge is to produce a model (in code) that will answer the following questions:

    1. What is the total journey time for the following direct routes (your model should indicate if the journey is invalid):
        1.1 Buenos Aires -> New York -> Liverpool
        1.2 Buenos Aires -> Casablanca -> Liverpool
        1.3 Buenos Aires -> Cape Town -> New York -> Liverpool -> Casablanca
        1.4 Buenos Aires -> Cape Town -> Casablanca
    2. Find the shortest journey time for the following routes:
        2.1 Buenos Aires -> Liverpool
        2.2 New York -> New York
    3. Find the number of routes from Liverpool to Liverpool with a maximum number of 3 stops.
    4. Find the number of routes from Buenos Aires to Liverpool where exactly 4 stops are made.
    5. Find the number of routes from Liverpool to Liverpool where the journey time is less than or equal to 25 days.
    
### OUTPUT:
```
    1.1 Journey time BA --> NY --> LP
    >>>Processor.get_journey_time(graph, ['BA', 'NY', 'LP'])
    10
    ........................
    1.2 Journey time BA --> CB --> LP
    >>>Processor.get_journey_time(graph, ['BA', 'CB', 'LP'])
    8
    ........................
    1.3 Journey time BA --> CT --> NY --> LP --> CB
    >>>Processor.get_journey_time(graph, ['BA', 'CT', 'NY', 'LP', 'CB'])
    19
    ........................
    1.4 Journey time BA --> CT --> CB
    >>>Processor.get_journey_time(graph, ['BA', 'CT', 'CB'])
    None
    ........................
    2.1 Shortest route BA --> LP 
    >>>Processor.find_shortest_route(graph, 'BA', 'LP')
    (8, ['BA', 'CB', 'LP'])
    ........................
    2.2 Shortest route NY --> NY
    >>>Processor.find_shortest_route(graph, 'NY', 'NY')
    (18, ['NY', 'LP', 'CT', 'NY'])
    ........................
    3. All routes with max 3 stops LP --> LP
    >>>Processor.find_routes(graph, 'LP', 'LP', relate='<=', limit=3)
    [['LP', 'CB', 'LP'], ['LP', 'CT', 'NY', 'LP']]
    ........................
    4. All routes with exactly 4 stops BA --> LP
    >>>Processor.find_routes(graph, 'BA', 'LP', condition=lambda l: len(l) == 4)
    [['BA', 'CT', 'NY', 'LP']]
    ........................
    5. All routes with max journey time of 25 days LP --> LP
    >>>Processor.find_routes_limit_journey_time(graph, 'LP', 'LP', 25)
    [(6, ['LP', 'CB', 'LP']), (21, ['LP', 'CB', 'CT', 'NY', 'LP']), (18, ['LP', 'CT', 'NY', 'LP'])]
```    

### Testing And Coverage
```
(venv)➜  router git:(master) nosetests --with-coverage --cover-html
...................
Name                  Stmts   Miss  Cover
-----------------------------------------
router.py                 0      0   100%
router/digraph.py        15      0   100%
router/processor.py      35      0   100%
router/utils.py          69      4    94%
-----------------------------------------
TOTAL                   119      4    97%
----------------------------------------------------------------------
Ran 19 tests in 0.013s

```