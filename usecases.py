from router.digraph import Digraph
from router.processor import Processor

if __name__ == '__main__':
    """
    REQUIREMENTS:

    The challenge is to produce a model (in code) that will answer the following questions:

    1. What is the total journey time for the following direct routes (your model should indicate if the journey is invalid):
        1.1 Buenos Aires -> New York -> Liverpool
        1.2 Buenos Aires -> Casablanca -> Liverpool
        1.3 Buenos Aires -> Cape Town -> New York -> Liverpool -> Casablanca
        1.4 Buenos Aires -> Cape Town -> Casablanca
    2. Find the shortest journey time for the following routes:
        2.1 Buenos Aires -> Liverpool
        2.2 New York -> New York
    3. Find the number of routes from Liverpool to Liverpool with a maximum number of 3 stops.
    4. Find the number of routes from Buenos Aires to Liverpool where exactly 4 stops are made.
    5. Find the number of routes from Liverpool to Liverpool where the journey time is less than or equal to 25 days.
    """
    graph = Digraph()

    for node in ['NY', 'LP', 'CB', 'CT', 'BA']:
        graph.add_node(node)

    graph.add_edge('BA', 'NY', 6)
    graph.add_edge('BA', 'CB', 5)
    graph.add_edge('BA', 'CT', 4)
    graph.add_edge('NY', 'LP', 4)
    graph.add_edge('LP', 'CB', 3)
    graph.add_edge('LP', 'CT', 6)
    graph.add_edge('CB', 'LP', 3)
    graph.add_edge('CB', 'CT', 6)
    graph.add_edge('CT', 'NY', 8)

    print('1.1 Journey time BA --> NY --> LP')
    print(">>>Processor.get_journey_time(graph, ['BA', 'NY', 'LP'])")
    print(Processor.get_journey_time(graph, ['BA', 'NY', 'LP']))
    print('........................')
    print('1.2 Journey time BA --> CB --> LP')
    print(">>>Processor.get_journey_time(graph, ['BA', 'CB', 'LP'])")
    print(Processor.get_journey_time(graph, ['BA', 'CB', 'LP']))
    print('........................')
    print('1.3 Journey time BA --> CT --> NY --> LP --> CB')
    print(">>>Processor.get_journey_time(graph, ['BA', 'CT', 'NY', 'LP', 'CB'])")
    print(Processor.get_journey_time(graph, ['BA', 'CT', 'NY', 'LP', 'CB']))
    print('........................')
    print('1.4 Journey time BA --> CT --> CB')
    print(">>>Processor.get_journey_time(graph, ['BA', 'CT', 'CB'])")
    print(Processor.get_journey_time(graph, ['BA', 'CT', 'CB']))
    print('........................')

    print('2.1 Shortest route BA --> LP ')
    print(">>>Processor.find_shortest_route(graph, 'BA', 'LP')")
    print(Processor.find_shortest_route(graph, 'BA', 'LP'))
    print('........................')
    print('2.2 Shortest route NY --> NY')
    print(">>>Processor.find_shortest_route(graph, 'NY', 'NY')")
    print(Processor.find_shortest_route(graph, 'NY', 'NY'))
    print('........................')

    print('3. All routes with max 3 stops LP --> LP')
    # print('For reference, all routes LP --> LP')
    # print(Processor.find_routes(graph, 'LP', 'LP'))
    print(">>>Processor.find_routes(graph, 'LP', 'LP', relate='<=', limit=3)")
    print(Processor.find_routes(graph, 'LP', 'LP', relate='<=', limit=3))
    print('........................')

    print('4. All routes with exactly 4 stops BA --> LP')
    print(">>>Processor.find_routes(graph, 'BA', 'LP', condition=lambda l: len(l) == 4)")
    print(Processor.find_routes(graph, 'BA', 'LP', condition=lambda l: len(l) == 4))
    # print('For reference, all routes BA --> LP')
    # print(Processor.find_routes(graph, 'BA', 'LP'))
    print('........................')

    print('5. All routes with max journey time of 25 days LP --> LP')
    print(">>>Processor.find_routes_limit_journey_time(graph, 'LP', 'LP', 25)")
    print(Processor.find_routes_limit_journey_time(graph, 'LP', 'LP', 25))
    print('........................')
    # print('All routes with max 20 days LP --> LP')
    # print('All routes LP --> LP')
    # print(Processor.find_routes(graph, 'LP', 'LP'))
    # print('All routes with journey times LP --> LP')
    # print(Processor.find_routes_limit_journey_time(graph, 'LP', 'LP'))
    # print('All routes with max journey time of 20 days LP --> LP')
    # print(Processor.find_routes_limit_journey_time(graph, 'LP', 'LP', 20))
    # print('........................')
    """
    OUTPUT
    1.1 Journey time BA --> NY --> LP
    10
    ........................
    1.2 Journey time BA --> CB --> LP
    8
    ........................
    1.3 Journey time BA --> CT --> NY --> LP --> CB
    19
    ........................
    1.4 Journey time BA --> CT --> CB
    None
    ........................
    2.1 Shortest route BA --> LP
    (8, ['BA', 'CB', 'LP'])
    ........................
    2.2 Shortest route NY --> NY
    (18, ['NY', 'LP', 'CT', 'NY'])
    ........................
    3. All routes with max 3 stops LP --> LP
    [['LP', 'CB', 'LP'], ['LP', 'CT', 'NY', 'LP']]
    ........................
    4. All routes with exactly 4 stops BA --> LP
    [['BA', 'CT', 'NY', 'LP']]
    ........................
    5. All routes with max journey time of 25 days LP --> LP
    [(6, ['LP', 'CB', 'LP']), (21, ['LP', 'CB', 'CT', 'NY', 'LP']), (18, ['LP', 'CT', 'NY', 'LP'])]
    ........................
    """
