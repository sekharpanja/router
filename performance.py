import timeit

from router.processor import Processor
from router.heapify import dijkstra_pqueue
from router.digraph import Digraph

edges = [
    ('BA', 'NY', 6),
    ('BA', 'CB', 5),
    ('BA', 'CT', 4),
    ('NY', 'LP', 4),
    ('LP', 'CB', 3),
    ('LP', 'CT', 6),
    ('CB', 'LP', 3),
    ('CB', 'CT', 6),
    ('CT', 'NY', 8)
]

graph = Digraph()

for node in ['NY', 'LP', 'CB', 'CT', 'BA']:
    graph.add_node(node)

graph.add_edge('BA', 'NY', 6)
graph.add_edge('BA', 'CB', 5)
graph.add_edge('BA', 'CT', 4)
graph.add_edge('NY', 'LP', 4)
graph.add_edge('LP', 'CB', 3)
graph.add_edge('LP', 'CT', 6)
graph.add_edge('CB', 'LP', 3)
graph.add_edge('CB', 'CT', 6)
graph.add_edge('CT', 'NY', 8)

def test_proc():
    Processor.find_shortest_route(graph, 'BA', 'LP')

def test_heap():
    dijkstra_pqueue(edges, "BA", "LP")

print "proc      :",timeit.Timer('f()', 'from __main__ import test_proc as f').timeit(1000000)
print "heap     :",timeit.Timer('f()', 'from __main__ import test_heap as f').timeit(1000000)
