"""
Digraph entity, representing a Directional Graph
"""
from collections import defaultdict


class Digraph(object):
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.weights = {}

    def add_node(self, value):
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, distance):
        if from_node not in self.nodes:
            self.add_node(from_node)
        if to_node not in self.nodes:
            self.add_node(to_node)

        self.edges[from_node].append(to_node)
        self.weights[(from_node, to_node)] = distance
