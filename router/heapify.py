from collections import defaultdict
from heapq import heappop, heappush


def dijkstra_pqueue(edgs, source, dest):
    graph = defaultdict(list)
    for left_vertex, right_vertex, cost in edgs:
        graph[left_vertex].append((cost, right_vertex))

    node_queue, seen = [(0, source, ())], set()
    while node_queue:
        cost, v1, path = heappop(node_queue)
        if v1 not in seen:
            seen.add(v1)
            path = (v1, path)
            if v1 == dest:
                return cost, path

            for cost, v2 in graph.get(v1, ()):
                if v2 not in seen:
                    heappush(node_queue, (cost+cost, v2, path))

    return float("inf")

if __name__ == "__main__":
    edges = [
        ('BA', 'NY', 6),
        ('BA', 'CB', 5),
        ('BA', 'CT', 4),
        ('NY', 'LP', 4),
        ('LP', 'CB', 3),
        ('LP', 'CT', 6),
        ('CB', 'LP', 3),
        ('CB', 'CT', 6),
        ('CT', 'NY', 8)
    ]

    print "=== Dijkstra With Priority Queue==="
    print edges
    print "BA -> LP:"
    print dijkstra_pqueue(edges, "BA", "LP")
