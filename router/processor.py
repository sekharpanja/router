"""
This module defines all APIs for extracting journey times, shortest paths, all possible paths etc
"""
from collections import deque

from router.utils import dijkstra, total_weight, find_all_paths_circular, find_all_paths_normal


class Processor(object):
    @staticmethod
    def find_shortest_route(digraph, source, destination):
        """
        Uses Dijktra `algorithm https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm`_ to obtain shortest possible path
        from source to destination. It also deals with cyclic requests.
        Returns None, if destination is not reachable from origin
        :param digraph: a Digraph object upon which this operation is processes
        :param source: origin id within the Digraph, however it was defined
        :param destination: destination id within the Digraph
        :return: a tuple of number of days and path as a list of ports both origin and destination inclusive
        """
        visited, paths = dijkstra(digraph, source, destination)
        full_path = deque()

        if destination not in paths:
            return None
        _destination = paths[destination]

        while _destination != source:
            full_path.appendleft(_destination)
            _destination = paths[_destination]

        full_path.appendleft(source)
        full_path.append(destination)

        return visited[destination], list(full_path)

    @staticmethod
    def get_journey_time(digraph, path):
        """
        Calculates total journey time for given path within the Digraph
        :param digraph: Digraph object to inspect
        :param path: a list of port ids starting with source ending with destination
        :return: integer representing total journey time, None if path is not traveresible
        """
        return total_weight(digraph, path)

    @staticmethod
    def find_routes(digraph, source, destination, **kwargs):
        """
        Finds out all possible routes from source to destination, optionally limit number of ports
        :param digraph: Digraph to inspect
        :param source: source port
        :param destination: destination port
        :param kwargs: takes three args,
        'condition': a lambda signifying the limit condition or
        'relate': operator to apply: >, <, >=, <=, = and 'limit': number of ports to limit to
        note that 'condition' takes precedence over others
        :return:  list of all paths
        """
        import operator
        ops = {'>': operator.gt,
               '<': operator.lt,
               '>=': operator.ge,
               '<=': operator.le,
               '=': operator.eq}

        limit_condition = None
        if 'condition' in kwargs:
            limit_condition = kwargs['condition']
        elif 'limit' in kwargs and 'relate' in kwargs:
            limit_condition = lambda n: ops[kwargs['relate']](len(n), kwargs['limit'])

        if source == destination:
            paths = find_all_paths_circular(digraph, source, destination, 0, limit_condition)
            for path in paths:
                path.insert(0, source)  # add source/destination node id at the begenning for completeness
        else:
            paths = find_all_paths_normal(digraph, source, destination, limit_condition)

        return paths

    @staticmethod
    def find_routes_limit_journey_time(digraph, source, destination, max_journey_time=None):
        """
        Returns all possible routes from source to destination, cyclic inclusive,
        with option to limit journey time in days
        :param digraph: Digraph to inspect
        :param source: source port
        :param destination: destination port
        :param max_journey_time: maximum journey time in days
        :return: list of tuples, total journey time in days and path as list of ports
        """
        paths_and_weights = [(total_weight(digraph, path), path)
                             for path
                             in Processor.find_routes(digraph, source, destination)]
        return list(filter((lambda t: max_journey_time is None or t[0] <= max_journey_time), paths_and_weights))
