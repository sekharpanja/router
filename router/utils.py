
def dijkstra(digraph, source, destination):
    """
    implementation of Dijksta algorithm, including cyclic routes
    :param digraph: Digraph object
    :param source: source node
    :param destination: destination node
    :return: two dicts,
    each node with its lowest weight from source and
    shortest path from source to each traversible node as tuple
    """
    visited = {source: 0}
    path = {}

    nodes = list(digraph.nodes)
    if source == destination:
        nodes.append(destination)

    while nodes:
        min_node = None

        for node in nodes:
            if node in visited:
                if min_node is None:
                    min_node = node
                elif visited[node] < visited[min_node]:
                    min_node = node
        if min_node is None:
            break

        nodes.remove(min_node)
        current_weight = visited[min_node]

        for edge in digraph.edges[min_node]:
            try:
                weight = current_weight + digraph.weights[(min_node, edge)]
            except KeyError:
                continue
            if edge not in visited or visited[edge] == 0 or weight < visited[edge]:
                visited[edge] = weight
                path[edge] = min_node

    return visited, path


def total_weight(digraph, path):
    """
    Inspects Digraph and calculates total weight of a path
    :param digraph:
    :param path:
    :return: total weight as integer
    """
    total_journey_time = 0
    edge_no = 0
    for edge in path:
        edge_no += 1
        for next_edge in path[edge_no:]:
            try:
                total_journey_time += digraph.weights[(edge, next_edge)]
            except KeyError:
                return None
            break
    return total_journey_time


def find_all_paths_normal(digraph, source, destination, condition=None, path=[]):
    """
    inspects the Digraph and retreives all possible paths from source to destination, optionally applying a
    condition to limit number of items in the path
    :param digraph: Digraph object
    :param source: source node
    :param destination: destination node
    :param condition: lambda as condition to apply on list
    :param path: this mutable default arg is used to fill path while recuresively examining the Digraph
    :return: list of paths
    """
    path = path + [source]
    if source == destination:
        return [path]
    if source not in digraph.nodes:
        return []
    paths = []
    for node in digraph.edges[source]:
        if node not in path:
            newpaths = find_all_paths_normal(digraph, node, destination, condition, path)
            for newpath in newpaths:
                if not condition or condition(newpath):
                    paths.append(newpath)
    return paths


def find_all_paths_circular(digraph, source, destination, count=0, condition=None, path=[]):
    """
    a variation of find_all_paths_normal, to cope with cyclic route.
    inspects the Digraph and retreives all possible paths from source to destination, optionally applying a
    condition to limit number of items in the path
    :param digraph: Digraph object
    :param source: source node
    :param destination: destination node
    :param count: count to identify first execution, ignore while calling from outside
    :param condition: lambda as condition to apply on list
    :param path: this mutable default arg is used to fill path while recuresively examining the Digraph
    :return: list of paths
    """
    if count > 0:
        path = path + [source]
    if count > 0 and source == destination:
        return [path]
    if source not in digraph.nodes:
        return []
    paths = []
    for node in digraph.edges[source]:
        if node not in path:
            count += 1
            newpaths = find_all_paths_circular(digraph, node, destination, count, condition, path)
            for newpath in newpaths:
                if not condition or condition(newpath):
                    paths.append(newpath)
    return paths
